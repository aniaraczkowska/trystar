<?php

use Sda\Trystar\Config\Config;
use Sda\Trystar\Controller\ApiController;
use Sda\Trystar\Db\DbConnection;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Response\Response;

require_once(__DIR__ . '/../vendor/autoload.php');

$api = new ApiController(
    new Request(),
    new Response(),
    new DbConnection(Config::$connectionParams)
);

$api->run();