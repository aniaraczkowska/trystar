(function () {

    var w = window.innerWidth - 22;
    var h = window.innerHeight - 22;

    var canvas = document.getElementById("crossroad");
    canvas.width = w;
    canvas.height = h;
    var ctx = canvas.getContext("2d");
    
    //grass-------------------------------------
    ctx.fillStyle = "#BFE084";
    ctx.fillRect(0, 0, w, h);
    
    //roads----------------------------------------
    ctx.fillStyle = "#474A47";
    ctx.fillRect(0, h / 2 - 150, w, 300);
    ctx.fillStyle = "#474A47";
    ctx.fillRect(w / 2 - 150, 0, 300, h);
    
    //line vertical
    for(var i=0; i<h;i=i+40){
    ctx.beginPath();
      ctx.moveTo(w/2, i);
      ctx.lineTo(w/2, i+20);
      ctx.lineWidth = 3;
      ctx.strokeStyle = "white";
      ctx.stroke();
  }
     //line horizontal
     for(var j=0;j<w;j=j+40){
      ctx.beginPath();
      ctx.moveTo(j, h/2);
      ctx.lineTo(j+20, h/2);
      ctx.lineWidth = 3;
      ctx.strokeStyle = "white";
      ctx.stroke();
     }
    //zebra------------------------------------
    for(var m=w/2-150;m<w/2+150;m=m+28.58) {
    ctx.fillStyle = "white";
    ctx.fillRect(m, h / 2 - 300, 14.29, 80);
    }
    
    for(var m=w/2-150;m<w/2+150;m=m+28.58) {
    ctx.fillStyle = "white";
    ctx.fillRect(m, h / 2 +220, 14.29, 80);
    }
    
    for(var m=h/2-150;m<h/2+150;m=m+28.58) {
    ctx.fillStyle = "white";
    ctx.fillRect(w/2-300,m, 80,14.29);
    }
    
    for(var m=h/2-150;m<h/2+150;m=m+28.58) {
    ctx.fillStyle = "white";
    ctx.fillRect(w/2+220,m,80, 14.29);
    }
    //semaphores-----------------------------
    function semaphore(width, height, posX, posY, rotate, light1, light2, light3) {
        var pH1 = 0, pH2 = 0, pH3 = 0, pV1 = 0, pV2 = 0, pV3 = 0;
        if (rotate === "hor") {
            pH1 = 5;
            pH2 = 50;
            pH3 = 100 - 5;
        }

        if (rotate === "ver") {
            pV1 = 5;
            pV2 = 50;
            pV3 = 100 - 5;
        }

        ctx.fillStyle = "black";
        ctx.fillRect(w / 2 + posX, h / 2 + posY, width, height);

        ctx.beginPath();
        ctx.fillStyle = light1;
        ctx.arc(w / 2 + posX + 25 + pH1, h / 2 + posY + 25 + pV1, 20, 0, 2 * Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = light2;
        ctx.arc(w / 2 + posX + 25 + pH2, h / 2 + posY + 25 + pV2, 20, 0, 2 * Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.fillStyle = light3;
        ctx.arc(w / 2 + posX + 25 + pH3, h / 2 + posY + 25 + pV3, 20, 0, 2 * Math.PI);
        ctx.fill();
    }

    function sem1lights(light1, light2, light3) {
        semaphore(50, 150, 160, 160, "ver", light1, light2, light3);
    }

    function sem2lights(light1, light2, light3) {
        semaphore(50, 150, -160 - 50, -160 - 150, "ver", light1, light2, light3);
    }

    function sem3lights(light1, light2, light3) {
        semaphore(150, 50, 160, -160 - 50, "hor", light1, light2, light3);
    }

    function sem4lights(light1, light2, light3) {
        semaphore(150, 50, -160 - 150, 160, "hor", light1, light2, light3);
    }

    sem1lights("#474A47", "#474A47", "#474A47");
    sem2lights("#474A47", "#474A47", "#474A47");
    sem3lights("#474A47", "#474A47", "#474A47");
    sem4lights("#474A47", "#474A47", "#474A47");

}());
