<?php

namespace Sda\Trystar\Config;

class Config
{
    const KEY_FROM_HEADER='12345678901234567890123456789012';
    public static $connectionParams = array(
        'dbname' => 'trystar',
        'user' => 'root',
        'password' => '',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
        'charset' => 'utf8'
    );
}