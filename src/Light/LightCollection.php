<?php

namespace Sda\Trystar\Light;

use Sda\Trystar\TypedCollection;


class LightCollection extends TypedCollection
{

    public function __construct()
    {
        $this->setItemType(Light::class);
    }

}